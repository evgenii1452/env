up: docker-up

init: \
	docker-clear \
	clone-back \
	clone-front \
	hosts \
	docker-up \
	setup-backend-env \
	setup-composer \
	setup-nuxt \
	get-permissions \
	pause \
	db-init \
	success-msg

update: \
	git-pull \
	docker-restart \
	setup-composer \
	build-frontend \
	get-permissions \
	success-msg



docker-clear:
	echo "Остановка существующих контейнеров проекта"
	docker-compose down --remove-orphans

docker-restart:
	docker-compose down
	docker-compose up -d --build

docker-up:
	echo "Инициализация контейнеров"
	docker-compose up -d --build

pause:
	sleep 10

setup-backend-env:
	echo "Распаковка конфигурационных файлов"
	(cp -f ./docker/PHP/.env ../back/.env)

setup-composer:
	echo "Установка зависимостей"
	docker exec -w /var/www/back tp_php-fpm composer install

setup-nuxt:
	echo "Установка зависимостей yarn"
	docker exec -w /var/www/front tp_frontend yarn install
	docker exec -w /var/www/front tp_frontend yarn build

build-frontend:
	docker exec -w /var/www/front tp_frontend yarn build

get-permissions:
	@echo "Выдача прав доступа для каталогов с динамическим содержимым"
	sudo chmod -R 777 ../front
	sudo chmod -R 777 ../back

git-pull:
	cd ../front && git pull origin master
	cd ../back && git pull origin master
	cd ../env

clone-back:
	git clone https://gitlab.com/evgenii1452/back.git ../back

clone-front:
	git clone https://gitlab.com/evgenii1452/front.git ../front

db-init:
	echo "Инициализация БД"
	docker exec -i tp_mysql mysql -utrif -ptrif eto_baza < ./docker/dumps/dump.sql

success-msg:
	echo "Проект успешно развернут!"

nginx-hosts:
	docker exec -i tp_nginx echo "127.0.0.1 back.trif" >> /etc/hosts
	docker exec -i tp_nginx echo "127.0.0.1 front.trif" >> /etc/hosts

hosts:
	echo "192.168.42.2 back.trif" >> /etc/hosts
	echo "192.168.42.10 front.trif" >> /etc/hosts

exec-front:
	sudo docker exec -it tp_frontend bash
exec-back:
	sudo docker exec -it tp_php-fpm bash
